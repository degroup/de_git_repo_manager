""" unit tests for de.git_repo_manager portion. """
import os

# noinspection PyPackageRequirements
import pytest
import shutil
import tempfile

from unittest.mock import patch
from conftest import skip_gitlab_ci

from ae.base import (
    BUILD_CONFIG_FILE, DOCS_FOLDER, PY_EXT, PY_INIT, TEMPLATES_FOLDER, TESTS_FOLDER,
    in_wd, norm_name, norm_path, read_file, write_file)
from ae.paths import path_items

from de.setup_project import (
    APP_PRJ, MODULE_PRJ, PACKAGE_PRJ, PARENT_FOLDERS, PARENT_PRJ, REQ_DEV_FILE_NAME, ROOT_PRJ,
    TPL_PACKAGE_NAME_PREFIX)

import de.git_repo_manager
# noinspection PyProtectedMember
from de.git_repo_manager import (
    ARG_ALL, COMMIT_MSG_FILE_NAME, MAIN_BRANCH, OUTSOURCED_FILE_NAME_PREFIX, OUTSOURCED_MARKER, REMOTE_CLASS_NAMES,
    TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID, TEMPLATE_PLACEHOLDER_ARGS_SUFFIX,
    TEMPLATE_PLACEHOLDER_ID_PREFIX, TEMPLATE_PLACEHOLDER_ID_SUFFIX, TPL_FILE_NAME_PREFIX, TPL_IMPORT_NAME_PREFIX,
    TPL_PACKAGES, TPL_STOP_CNV_PREFIX, TPL_VERSION_OPTION_SUFFIX, REGISTERED_ACTIONS, REGISTERED_TPL_PROJECTS,
    _action, _act_callable, _act_spec, _available_actions, _check_arguments, _cl,
    _debug_or_verbose, _exit_error, _expected_args, _git_add, _git_clone, _git_commit, _git_diff, _git_status,
    _patch_outsourced, _portion_args_package_names, _prepare_act_exec, _project_parent_path,
    _register_args_and_local_templates, _renew_prj_dir, _renew_project, _repo_domain, _template_projects,
    bump_file_version, deploy_template, find_modules, package_project_path, patch_string, project_dev_vars,
    refresh_templates, replace_with_file_content_or_default,
    cae, GitlabCom)


@pytest.fixture
def changed_repo_path():
    """ provide an git repository with uncommitted changes, yielding the project's temporary working tree root path. """
    with tempfile.TemporaryDirectory() as tmp_path:
        with in_wd(tmp_path):
            _cl(969, "git init")
            write_file(os.path.join(tmp_path, 'delete.txt'), "--will be deleted")
            write_file(os.path.join(tmp_path, 'changed.py'), "# will be changed")
            _cl(969, "git add -A")
            _cl(969, "git commit -m 'git commit message'")

            write_file(os.path.join(tmp_path, 'added.cfg'), "# added/staged to repo")
            os.remove(os.path.join(tmp_path, 'delete.txt'))
            write_file(os.path.join(tmp_path, 'changed.py'), "# got changed")

            yield tmp_path


@pytest.fixture
def empty_repo_path():
    """ provide an empty git repository, yielding the path of the project's temporary working tree root. """
    with tempfile.TemporaryDirectory() as tmp_path:
        with in_wd(tmp_path):
            _cl(999, "git init")
            yield tmp_path


@pytest.fixture
def mocked_app_options():
    """ prevent argument parsing via cae.get_option('domain') called by _repo_domain(). """
    ori_get_arg = cae.get_argument
    ori_get_opt = cae.get_option

    mocked_options = {}
    cae.get_argument = cae.get_option = lambda opt: mocked_options.get(opt, f"{opt}_patched_get_option_value")

    yield mocked_options

    cae.get_argument = ori_get_arg
    cae.get_option = ori_get_opt


@pytest.fixture
def patched_exit_call_wrapper():
    """ count _exit_error() calls and ensure return of function to be tested. """
    exit_called = 0

    class _ExitCaller(Exception):
        """ exception to recognize and simulate app exit for function to be tested. """

    def _exit_(*args):
        nonlocal exit_called
        exit_called += 1
        assert len(args) in (1, 2), f"called patched exit_error{args} with incorrect number of args ({len(args)})"
        raise _ExitCaller

    def _call_wrapper(fun, *args, **kwargs):
        try:
            ret = fun(*args, **kwargs)
        except _ExitCaller:
            ret = None
        return ret

    # noinspection PyProtectedMember
    ori_fun = de.git_repo_manager._exit_error
    de.git_repo_manager._exit_error = _exit_

    yield _call_wrapper

    # noinspection PyProtectedMember
    de.git_repo_manager._exit_error = ori_fun
    assert exit_called


@pytest.fixture
def reset_patched_globals():
    """ reset git_repo_manager module after test that mocks/patches globals like INI_PEV/ACTION_NAME/.... """
    # pretest-check on empty/initial global values, reset lists/dicts inplace to empty/initial values after test
    err_msg = "a previous test did not restore globals, to fix add reset_patched_globals fixture to it"
    assert not de.git_repo_manager.INI_PDV and not de.git_repo_manager.PRJ_PDV, err_msg
    assert not de.git_repo_manager.ACTION_ARGS and not de.git_repo_manager.ACTION_NAME, err_msg
    assert not de.git_repo_manager.REGISTERED_TPL_PROJECTS
    assert not de.git_repo_manager.REMOTE_REPO

    yield

    de.git_repo_manager.INI_PDV.clear()
    de.git_repo_manager.PRJ_PDV.clear()
    de.git_repo_manager.ACTION_NAME = ""
    de.git_repo_manager.ACTION_ARGS.clear()
    de.git_repo_manager.REGISTERED_TPL_PROJECTS.clear()
    de.git_repo_manager.REMOTE_REPO = None


# noinspection PyUnusedLocal
class TestHelpers:
    """ test public helper functions. """
    def test_bump_file_version_invalid_file(self):
        err = bump_file_version('::invalid_file_name::')
        assert err

    def test_bump_file_version_empty_file(self):
        tst_file = 'test_bump_version' + PY_EXT
        try:
            write_file(tst_file, "")
            err = bump_file_version(tst_file)
            assert err
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_bump_file_version_multi_version(self):
        tst_file = 'test_bump_version' + PY_EXT
        try:
            write_file(tst_file, f"__version__ = '1.2.3'{os.linesep}__version__ = '2.3.4'")
            err = bump_file_version(tst_file)
            assert err
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_bump_file_version_major(self):
        tst_file = 'test_bump_version' + PY_EXT
        try:
            write_file(tst_file, f"__version__ = '1.2.3'{os.linesep}")

            err = bump_file_version(tst_file, version_part=1)

            assert not err

            content = read_file(tst_file)
            assert "__version__ = '1.2.3'" not in content
            assert "__version__ = '2.2.3'" in content
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_bump_file_version_minor(self):
        tst_file = 'test_bump_version' + PY_EXT
        try:
            write_file(tst_file, f"__version__ = '1.2.3'{os.linesep}{os.linesep}version = '2.3.4'")

            err = bump_file_version(tst_file, version_part=2)

            assert not err

            content = read_file(tst_file)
            assert "__version__ = '1.2.3'" not in content
            assert "__version__ = '1.3.3'" in content
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_bump_file_version_build(self):
        tst_file = 'test_bump_version' + PY_EXT
        try:
            write_file(tst_file, f"__version__ = '1.2.3'{os.linesep}version = '2.3.4'")

            err = bump_file_version(tst_file)

            assert not err

            content = read_file(tst_file)
            assert "__version__ = '1.2.3'" not in content
            assert "__version__ = '1.2.4'" in content
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_bump_file_version_remove_suffix(self):
        tst_file = 'test_bump_version' + PY_EXT
        try:
            write_file(tst_file, f"__version__ = '1.2.3pre'{os.linesep}version = '2.3.4'")

            err = bump_file_version(tst_file, version_part=2)

            assert not err

            content = read_file(tst_file)
            assert "__version__ = '1.2.3'" not in content
            assert "__version__ = '1.3.3'" in content
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_bump_file_version_keeping_comment(self):
        tst_file = 'test_bump_version' + PY_EXT
        comment_str = "  # comment string"
        try:
            write_file(tst_file, f"__version__ = '1.2.3'{comment_str}{os.linesep}version = '2.3.4'")

            err = bump_file_version(tst_file, version_part=1)

            assert not err

            content = read_file(tst_file)
            assert f"__version__ = '2.2.3'{comment_str}" in content
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_declaration_of_template_vars(self):
        assert OUTSOURCED_FILE_NAME_PREFIX
        assert OUTSOURCED_MARKER
        assert TPL_FILE_NAME_PREFIX
        assert TEMPLATE_PLACEHOLDER_ID_PREFIX
        assert TEMPLATE_PLACEHOLDER_ID_SUFFIX
        assert TEMPLATE_PLACEHOLDER_ARGS_SUFFIX
        assert TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID

    def test_deploy_template_unchanged_in_sub_dir(self, reset_patched_globals):
        join = os.path.join
        parent_dir = join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        src_dir = join(parent_dir, 'tpl_src_prj_dir')
        tpl_dir = join(src_dir, TEMPLATES_FOLDER)
        sub_dir_folder = 'sub_dir'
        tpl_sub_dir = join(tpl_dir, sub_dir_folder)
        file_name = 'unchanged_template.ext'
        src_file = join(tpl_sub_dir, file_name)
        content = "template file content"
        de.git_repo_manager.PRJ_PDV['project_path'] = dst_dir = join(parent_dir, 'dst')

        dst_files = set()
        try:
            os.makedirs(tpl_sub_dir)
            write_file(src_file, content)
            os.makedirs(dst_dir)

            deploy_template(src_file, sub_dir_folder, "", de.git_repo_manager.PRJ_PDV, dst_files=dst_files)

            dst_file = join(dst_dir, sub_dir_folder, file_name)
            assert os.path.isfile(dst_file)
            assert read_file(dst_file) == content
            assert norm_path(dst_file) in dst_files

        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_deploy_template_otf_in_sub_dir(self, reset_patched_globals):
        join = os.path.join
        parent_dir = join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        prj_dir = join(parent_dir, 'prj_with_otf_tpl')
        tpl_dir = join(prj_dir, TEMPLATES_FOLDER)
        sub_dir_folder = 'sub_dir'
        tpl_sub_dir = join(tpl_dir, sub_dir_folder)
        file_name = OUTSOURCED_FILE_NAME_PREFIX + 'changed_template' + PY_EXT
        src_file = join(tpl_sub_dir, file_name)
        content = "# template file content"
        de.git_repo_manager.PRJ_PDV['project_path'] = dst_dir = join(parent_dir, 'dst')
        patcher = "patching package id to be added to patched destination file"

        dst_files = set()
        try:
            os.makedirs(tpl_sub_dir)
            write_file(src_file, content)
            os.makedirs(dst_dir)

            deploy_template(src_file, sub_dir_folder, patcher, de.git_repo_manager.PRJ_PDV, dst_files=dst_files)

            dst_file = join(dst_dir, sub_dir_folder, file_name[len(OUTSOURCED_FILE_NAME_PREFIX):])
            assert os.path.isfile(dst_file)
            assert OUTSOURCED_MARKER in read_file(dst_file)
            assert patcher in read_file(dst_file)
            assert read_file(dst_file).endswith(content)
            assert norm_path(dst_file) in dst_files

        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_deploy_template_tpl_in_sub_dir(self, reset_patched_globals):
        join = os.path.join
        parent_dir = join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        prj_dir = join(parent_dir, 'tpl_src_root_dir')
        tpl_dir = join(prj_dir, TEMPLATES_FOLDER)
        sub_dir_folder = 'sub_dir'
        tpl_sub_dir = join(tpl_dir, sub_dir_folder)
        file_name = TPL_FILE_NAME_PREFIX + 'changed_template' + PY_EXT
        src_file = join(tpl_sub_dir, file_name)
        content = "# template file content created in {project_path}"
        de.git_repo_manager.PRJ_PDV['project_path'] = dst_dir = join(parent_dir, 'dst')
        patcher = "patching package id here not added to patched destination file"

        dst_files = set()
        try:
            os.makedirs(tpl_sub_dir)
            write_file(src_file, content)
            os.makedirs(dst_dir)

            deploy_template(src_file, sub_dir_folder, patcher, de.git_repo_manager.PRJ_PDV, dst_files=dst_files)

            dst_file = join(dst_dir, sub_dir_folder, file_name[len(TPL_FILE_NAME_PREFIX):])
            assert os.path.isfile(dst_file)
            assert OUTSOURCED_MARKER not in read_file(dst_file)
            assert patcher not in read_file(dst_file)
            assert read_file(dst_file).endswith(content.format(project_path=dst_dir))
            assert norm_path(dst_file) in dst_files

        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_deploy_template_otf_tpl_in_sub_dir(self, reset_patched_globals):
        join = os.path.join
        parent_dir = join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        prj_dir = join(parent_dir, 'prj_root_dir')
        tpl_dir = join(prj_dir, TEMPLATES_FOLDER)
        sub_dir_folder = 'sub_dir'
        tpl_sub_dir = join(tpl_dir, sub_dir_folder)
        file_name = OUTSOURCED_FILE_NAME_PREFIX + TPL_FILE_NAME_PREFIX + 'changed_template' + PY_EXT
        src_file = join(tpl_sub_dir, file_name)
        content = "# template file content created in {project_path}"
        de.git_repo_manager.PRJ_PDV['project_path'] = dst_dir = join(parent_dir, 'dst')
        patcher = "patching project name to be added to destination file"

        dst_files = set()
        try:
            os.makedirs(tpl_sub_dir)
            write_file(src_file, content)
            os.makedirs(dst_dir)

            deploy_template(src_file, sub_dir_folder, patcher, de.git_repo_manager.PRJ_PDV, dst_files=dst_files)

            dst_file = join(dst_dir, sub_dir_folder,
                            file_name[len(OUTSOURCED_FILE_NAME_PREFIX) + len(TPL_FILE_NAME_PREFIX):])
            assert os.path.isfile(dst_file)
            assert OUTSOURCED_MARKER in read_file(dst_file)
            assert patcher in read_file(dst_file)
            assert read_file(dst_file).endswith(content.format(project_path=dst_dir))
            assert norm_path(dst_file) in dst_files

        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_deploy_template_otf_stop_tpl(self, reset_patched_globals):
        join = os.path.join
        parent_dir = join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        prj_dir = join(parent_dir, 'prj_root_dir')
        prj_sub_dir = join(prj_dir, TEMPLATES_FOLDER)
        tpl_dir = join(prj_dir, TEMPLATES_FOLDER)
        sub_dir_folder = TEMPLATES_FOLDER
        tpl_sub_dir = join(tpl_dir, sub_dir_folder)
        file_name = OUTSOURCED_FILE_NAME_PREFIX + TPL_STOP_CNV_PREFIX + TPL_FILE_NAME_PREFIX + 'chg_template' + PY_EXT
        src_file = join(tpl_sub_dir, file_name)
        content = "# template file content created in {project_path}"
        de.git_repo_manager.PRJ_PDV['project_path'] = dst_dir = join(parent_dir, 'dst', TEMPLATES_FOLDER)
        patcher = "patcher"

        dst_files = set()
        try:
            os.makedirs(tpl_sub_dir)
            write_file(src_file, content)
            os.makedirs(dst_dir)

            deploy_template(src_file, sub_dir_folder, patcher, de.git_repo_manager.PRJ_PDV, dst_files=dst_files)

            dst_file = join(dst_dir, sub_dir_folder,
                            file_name[len(OUTSOURCED_FILE_NAME_PREFIX) + len(TPL_STOP_CNV_PREFIX):])
            assert os.path.isfile(dst_file)
            assert OUTSOURCED_MARKER in read_file(dst_file)
            assert patcher in read_file(dst_file)
            assert read_file(dst_file).endswith(content)
            assert norm_path(dst_file) in dst_files

        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_deploy_template_otf_existing_unlocked_because_marker(self, reset_patched_globals):
        join = os.path.join
        parent_dir = join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        prj_dir = join(parent_dir, 'project_dir')
        tpl_dir = join(prj_dir, TEMPLATES_FOLDER)
        file_name = OUTSOURCED_FILE_NAME_PREFIX + 'unlocked_template' + PY_EXT
        src_file = join(tpl_dir, file_name)
        content = f"# template file extra content"
        de.git_repo_manager.PRJ_PDV['project_path'] = dst_dir = join(parent_dir, 'dst')
        patcher = "patching package id to be added to destination file"

        dst_files = set()
        try:
            os.makedirs(tpl_dir)
            write_file(src_file, content)
            os.makedirs(dst_dir)
            dst_file = join(dst_dir, file_name[len(OUTSOURCED_FILE_NAME_PREFIX):])
            write_file(dst_file, OUTSOURCED_MARKER)

            deploy_template(src_file, "", patcher, de.git_repo_manager.PRJ_PDV, dst_files=dst_files)

            assert os.path.isfile(dst_file)
            assert OUTSOURCED_MARKER in read_file(dst_file)
            assert content in read_file(dst_file)
            assert patcher in read_file(dst_file)
            assert norm_path(dst_file) in dst_files

        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_deploy_template_otf_existing_locked_without_marker(self, reset_patched_globals):
        join = os.path.join
        parent_dir = join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        prj_dir = join(parent_dir, 'prj_root')
        tpl_dir = join(prj_dir, TEMPLATES_FOLDER)
        file_name = OUTSOURCED_FILE_NAME_PREFIX + 'locked_template' + PY_EXT
        src_file = join(tpl_dir, file_name)
        content = "# template file content"
        de.git_repo_manager.PRJ_PDV['project_path'] = dst_dir = join(parent_dir, 'dst')
        patcher = "patcher id or package name"

        dst_files = set()
        try:
            os.makedirs(tpl_dir)
            write_file(src_file, content)
            os.makedirs(dst_dir)
            dst_file = join(dst_dir, file_name[len(OUTSOURCED_FILE_NAME_PREFIX):])
            dst_content = "locked because not contains marker"
            write_file(dst_file, dst_content)

            deploy_template(src_file, "", patcher, de.git_repo_manager.PRJ_PDV, dst_files=dst_files)

            assert os.path.isfile(dst_file)
            assert OUTSOURCED_MARKER not in read_file(dst_file)
            assert read_file(dst_file) == dst_content
            assert patcher not in read_file(dst_file)
            assert norm_path(dst_file) in dst_files

        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_deploy_template_otf_locked_by_file(self, reset_patched_globals):
        join = os.path.join
        parent_dir = join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        prj_dir = join(parent_dir, 'prj_dir')
        tpl_dir = join(prj_dir, TEMPLATES_FOLDER)
        file_name = OUTSOURCED_FILE_NAME_PREFIX + 'locked_template' + PY_EXT
        src_file = join(tpl_dir, file_name)
        content = "# template file content"
        de.git_repo_manager.PRJ_PDV['project_path'] = dst_dir = join(parent_dir, 'dst')

        dst_files = set()
        try:
            os.makedirs(tpl_dir)
            write_file(src_file, content)
            os.makedirs(dst_dir)
            dst_file = join(dst_dir, file_name[len(OUTSOURCED_FILE_NAME_PREFIX):])
            write_file(dst_file + '.locked', "")

            deploy_template(src_file, "", "", de.git_repo_manager.PRJ_PDV, dst_files=dst_files)

            assert not os.path.isfile(dst_file)
            assert norm_path(dst_file) in dst_files

        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_deploy_template_tpl_locked_by_priority(self, reset_patched_globals):
        join = os.path.join
        parent_dir = join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        prj_dir = join(parent_dir, 'prj')
        tpl_dir = join(prj_dir, TEMPLATES_FOLDER)
        file_name = TPL_FILE_NAME_PREFIX + 'template' + PY_EXT
        src_file = join(tpl_dir, file_name)
        content = "# template file content"
        de.git_repo_manager.PRJ_PDV['project_path'] = dst_dir = join(parent_dir, 'dst')

        dst_files = set()
        try:
            os.makedirs(tpl_dir)
            write_file(src_file, content)
            os.makedirs(dst_dir)
            dst_file = join(dst_dir, file_name[len(TPL_FILE_NAME_PREFIX):])

            deploy_template(src_file, "", "", de.git_repo_manager.PRJ_PDV, dst_files=dst_files)

            assert os.path.isfile(dst_file)
            assert read_file(dst_file) == content
            assert norm_path(dst_file) in dst_files
            dst_files_len = len(dst_files)

            write_file(src_file, 'any OTHER content')

            # second deploy try from tpl prj with lower priority
            deploy_template(src_file, "", "", de.git_repo_manager.PRJ_PDV, dst_files=dst_files)

            assert os.path.isfile(dst_file)
            assert read_file(dst_file) == content
            assert dst_files_len == len(dst_files)

        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_find_modules_de_setup_project(self):
        modules = find_modules(os.getcwd())
        assert modules == ['setup' + PY_EXT]

        modules = find_modules(os.getcwd(), namespace_name='de')
        assert modules == ['git_repo_manager.py']

    def test_late_init_of_constants(self):
        assert REGISTERED_ACTIONS
        assert REMOTE_CLASS_NAMES
        assert TPL_PACKAGES

        assert not REGISTERED_TPL_PROJECTS      # init later by _register_args_and_local_templates() at action init

    @skip_gitlab_ci
    def test_package_project_path_local(self):
        assert package_project_path('ae.base')
        assert package_project_path('de.setup_project')
        assert package_project_path('gitlab')

    def test_package_project_path_invalid(self):
        assert not package_project_path("")
        assert not package_project_path("_never_ever_an_existing_package_import_name")
        assert not package_project_path("os")       # no project path for built-in modules (situated in lib/pythonX.Y/.)

    def test_patch_string_empty_args(self):
        assert patch_string("", {}, invalid_place_holder_id=lambda s: "") == ""

    def test_project_dev_vars(self, mocked_app_options, reset_patched_globals):
        namespace = 'xyz'
        mocked_app_options['group'] = "tst_grp"

        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        root_project_path = os.path.join(parent_dir, namespace)
        docs_dir = os.path.join(root_project_path, DOCS_FOLDER)
        common_dir = os.path.join(root_project_path, TEMPLATES_FOLDER)

        package_name = 'tst_pkg'
        package_dir = os.path.join(parent_dir, namespace + "_" + package_name)
        package_path = os.path.join(package_dir, namespace, package_name)
        package_extra_module_name = "extra_module_name"

        module_name = 'tst_mod'
        module_dir = os.path.join(parent_dir, namespace + "_" + module_name)
        try:
            os.makedirs(root_project_path)
            os.makedirs(docs_dir)
            os.makedirs(common_dir)
            write_file(os.path.join(root_project_path, REQ_DEV_FILE_NAME),
                       namespace + '_' + package_name + os.linesep + namespace + '_' + module_name)

            os.makedirs(package_path)
            write_file(os.path.join(package_path, PY_INIT), "ini_content = ''")
            write_file(os.path.join(package_path, package_extra_module_name + PY_EXT), "extra_content = ''")

            os.makedirs(module_dir)
            write_file(os.path.join(module_dir, module_name), "mod_content = ''")

            de.git_repo_manager.PRJ_PDV = pev = project_dev_vars(project_path=root_project_path)

            assert f"{namespace}.{package_name}" in pev['portions_import_names']
            assert f"{namespace}.{package_name}.{package_extra_module_name}" in pev['portions_import_names']
            assert f"{namespace}.{module_name}" in pev['portions_import_names']
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_refresh_templates_empty_args(self, reset_patched_globals):
        assert refresh_templates({}) == set()

    def test_refresh_templates_empty_project_type(self, reset_patched_globals):
        REGISTERED_TPL_PROJECTS[TPL_IMPORT_NAME_PREFIX] = {'path': 'tpl_empty_type_path'}
        REGISTERED_TPL_PROJECTS[TPL_IMPORT_NAME_PREFIX + 'project'] = {'path': 'tpl_prj_path'}

        assert refresh_templates({}) == set()

    def test_refresh_templates_test_registered(self, reset_patched_globals):
        join = os.path.join
        parent_dir = join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        namespace = "nsn"
        package_name = f"{namespace}_pkg_name"
        project_path = join(parent_dir, package_name)
        tpl_projects = [
            {'import_name': namespace,
             'path': join(parent_dir, namespace), 'version': '1.1.1'},
            {'import_name': TPL_IMPORT_NAME_PREFIX + 'package',
             'path': join(parent_dir, 'de_tpl_package'), 'version': '3.3.3'},
            {'import_name': TPL_IMPORT_NAME_PREFIX + 'project',
             'path': join(parent_dir, 'de_tpl_project'), 'version': '9.9.9'},
        ]

        try:
            _renew_prj_dir(namespace, package_name, project_path, PACKAGE_PRJ)
            # de.git_repo_manager.PRJ_PEV = project_env_vars(project_path=project_path)

            assert not refresh_templates(dict(project_path=project_path))   # first test w/o created template folders

            assert os.path.isdir(project_path)
            assert not os.path.isdir(join(project_path, DOCS_FOLDER))
            assert not os.path.isdir(join(project_path, TEMPLATES_FOLDER))
            assert os.path.isdir(join(project_path, TESTS_FOLDER))
            assert os.path.isfile(join(project_path, namespace, package_name[len(namespace) + 1:], PY_INIT))
            assert not os.path.isfile(join(project_path, BUILD_CONFIG_FILE))

            deep_sub_dir = join('deeper', 'even_deeper')
            file_for_all = 'file_for_all.ext'
            tpl_file_for_all = OUTSOURCED_FILE_NAME_PREFIX + TPL_FILE_NAME_PREFIX + file_for_all
            for tpl_reg in tpl_projects:
                tpl_path = join(tpl_reg['path'], TEMPLATES_FOLDER, deep_sub_dir)
                os.makedirs(tpl_path)
                write_file(join(tpl_path, tpl_file_for_all), tpl_reg['path'])
            tpl_file = join(project_path, deep_sub_dir, file_for_all)

            # 2nd test with template in all templates (root prj has highest priority)
            f_vars = dict(namespace_name=namespace, project_path=project_path, project_type=PACKAGE_PRJ,
                          TEMPLATES_FOLDER=TEMPLATES_FOLDER, tpl_projects=tpl_projects)
            assert refresh_templates(f_vars) == {norm_path(tpl_file)}

            assert os.path.isfile(tpl_file)
            content = read_file(tpl_file)
            assert tpl_projects[0]['path'] in content
            assert OUTSOURCED_MARKER in content

        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_refresh_templates_file_include_content(self, reset_patched_globals):
        join = os.path.join
        parent_dir = join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        tpl_prj_path = norm_path(join(parent_dir, 'tpl_prj'))
        tpl_file_name = "including_content.txt"
        tpl_file_path = join(tpl_prj_path, OUTSOURCED_FILE_NAME_PREFIX + TPL_FILE_NAME_PREFIX + tpl_file_name)
        version = '9.6.9999'
        tpl_projects = [{'import_name': TPL_IMPORT_NAME_PREFIX + 'project', 'path': tpl_prj_path, 'version': version}]
        included_file_name = norm_path(join(parent_dir, "inc.tst.file"))
        included_file_content = "replacement string"
        package_name = f"pkg_name"
        project_path = join(parent_dir, package_name)
        patched_file_name = join(project_path, tpl_file_name)
        cur_dir = os.getcwd()
        try:
            os.makedirs(project_path)
            os.makedirs(tpl_prj_path)

            tpl = f"{TEMPLATE_PLACEHOLDER_ID_PREFIX}{TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID}"
            tpl += f"{TEMPLATE_PLACEHOLDER_ID_SUFFIX}{included_file_name}{TEMPLATE_PLACEHOLDER_ARGS_SUFFIX}"
            write_file(tpl_file_path, tpl)
            write_file(included_file_name, included_file_content)

            os.chdir(project_path)
            assert refresh_templates(dict(tpl_projects=tpl_projects), ".") == set()

            patched = refresh_templates(dict(TEMPLATES_FOLDER="", tpl_projects=tpl_projects))
            os.chdir(cur_dir)

            assert patched == {norm_path(patched_file_name)}

            content = read_file(patched_file_name)
            assert included_file_content in content
            assert version in content
            assert "TEMPLATE_PLACEHOLDER_ID_PREFIX" not in content
            assert TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID not in content
            assert "TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID" not in content
            assert TEMPLATE_PLACEHOLDER_ID_SUFFIX not in content
            assert "TEMPLATE_PLACEHOLDER_ID_SUFFIX" not in content
            assert TEMPLATE_PLACEHOLDER_ARGS_SUFFIX not in content
            assert "TEMPLATE_PLACEHOLDER_ARGS_SUFFIX" not in content
        finally:
            os.chdir(cur_dir)
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_refresh_templates_file_include_default_and_with_pev_vars(self, mocked_app_options, reset_patched_globals):
        join = os.path.join
        mocked_app_options['group'] = "grp_tst_name"
        parent_dir = join(TESTS_FOLDER, PARENT_FOLDERS[0])
        tpl_prj_path = norm_path(join(parent_dir, 'tpl_prj'))
        tpl_file_name = "including_content.txt"
        tpl_file_path = join(tpl_prj_path, TEMPLATES_FOLDER,
                             OUTSOURCED_FILE_NAME_PREFIX + TPL_FILE_NAME_PREFIX + tpl_file_name)
        default = "default string"
        version = '6.699.987'
        tpl_projects = [{'import_name': TPL_IMPORT_NAME_PREFIX + 'project', 'path': tpl_prj_path, 'version': version}]
        de.git_repo_manager.REGISTERED_TPL_PROJECTS[TPL_IMPORT_NAME_PREFIX + 'project'] = tpl_projects[0]
        package_name = f"pack_name"
        project_path = join(parent_dir, package_name)
        patched_file = join(project_path, tpl_file_name)
        try:
            os.makedirs(project_path)
            write_file(os.path.join(project_path, package_name + PY_EXT), "__version__ = '9.6.3'")
            write_file(os.path.join(project_path, REQ_DEV_FILE_NAME), TPL_PACKAGE_NAME_PREFIX + 'project')
            os.makedirs(os.path.dirname(tpl_file_path))

            tpl = "{TEMPLATE_PLACEHOLDER_ID_PREFIX}{TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID}"
            tpl += "{TEMPLATE_PLACEHOLDER_ID_SUFFIX}"
            tpl += f"not_existing_included_file_name.ext,{default}"
            tpl += "{TEMPLATE_PLACEHOLDER_ARGS_SUFFIX}"
            write_file(tpl_file_path, tpl)

            assert refresh_templates(dict(tpl_projects=tpl_projects), ".") == set()

            assert refresh_templates(project_dev_vars(project_path=project_path)) == {norm_path(patched_file)}

            content = read_file(patched_file)
            assert default in content
            assert version in content
            assert "TEMPLATE_PLACEHOLDER_ID_PREFIX" not in content
            assert TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID not in content
            assert "TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID" not in content
            assert TEMPLATE_PLACEHOLDER_ID_SUFFIX not in content
            assert "TEMPLATE_PLACEHOLDER_ID_SUFFIX" not in content
            assert TEMPLATE_PLACEHOLDER_ARGS_SUFFIX not in content
            assert "TEMPLATE_PLACEHOLDER_ARGS_SUFFIX" not in content
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_replace_with_file_content_or_default(self):
        assert replace_with_file_content_or_default("") == ""

        def_val = "default_value"
        assert replace_with_file_content_or_default(f"_non_file,{def_val}") == def_val
        assert replace_with_file_content_or_default(f"_non_file,{def_val},{def_val}") == def_val + "," + def_val

        assert replace_with_file_content_or_default(f"_non_file,'{def_val[:3]}' + '{def_val[3:]}'") == def_val
        assert replace_with_file_content_or_default(f"_non_file,369") == 369
        with pytest.raises(ValueError):
            assert replace_with_file_content_or_default(f"_non_file,{'a': 111}")
        with pytest.raises(ValueError):
            assert replace_with_file_content_or_default(f"_non_file,{3: '111'}")
        assert replace_with_file_content_or_default(f"_non_file,{111}") == 111
        assert replace_with_file_content_or_default(f"_non_file,[111]") == [111]
        assert replace_with_file_content_or_default(f"_non_file,(1, 2, 3)") == (1, 2, 3)

        file_name = os.path.join(TESTS_FOLDER, "returned_file.txt")
        content = "file_content_to_be_returned"
        try:
            assert replace_with_file_content_or_default(f"{file_name},{def_val}") == def_val
            write_file(file_name, content)
            assert replace_with_file_content_or_default(file_name) == content
            assert replace_with_file_content_or_default(f"{file_name},{def_val}") == content
        finally:
            if os.path.exists(file_name):
                os.remove(file_name)


# noinspection PyUnusedLocal
class TestHiddenHelpersLocal:
    """ test private helper functions that doesn't need any authentication against git remote hosts. """
    def test_action_decorator(self, mocked_app_options, reset_patched_globals):
        def test_fun(*_args):
            """ test fun docstring """
            return _args

        try:
            dec = _action(APP_PRJ, MODULE_PRJ, kwarg1='1', kwarg2=2)
            assert callable(dec)
            assert callable(dec(test_fun))
            assert 'test_fun' in REGISTERED_ACTIONS
            assert APP_PRJ in REGISTERED_ACTIONS['test_fun']['project_types']
            assert MODULE_PRJ in REGISTERED_ACTIONS['test_fun']['project_types']
            assert REGISTERED_ACTIONS['test_fun']['docstring'] == " test fun docstring "

            cae.get_option = lambda opt: ""     # prevent argument parsing via cae
            de.git_repo_manager.ACTION_NAME = 'test_fun'
            de.git_repo_manager.INI_PDV['project_type'] = APP_PRJ
            assert dec(test_fun)() == ()

        finally:
            REGISTERED_ACTIONS.pop('test_fun', None)

    def test_act_callable(self, mocked_app_options):
        mocked_app_options['domain'] = ""
        assert callable(_act_callable('new_app'))
        assert callable(_act_callable('fork'))
        assert callable(_act_callable('xxx_yyy_zzz'))
        assert _act_callable('xxx_yyy_zzz') == cae.show_help

    def test_act_spec_local_act(self, mocked_app_options):
        mocked_app_options['domain'] = ""
        try:
            de.git_repo_manager.ACTION_NAME = 'new_app'
            spe = _act_spec()
            assert isinstance(spe, dict)
            assert spe
            assert 'project_types' in spe
            assert 'docstring' in spe
        finally:
            de.git_repo_manager.ACTION_NAME = ""

    def test_act_spec_remote_act(self, mocked_app_options):
        mocked_app_options['domain'] = ""
        try:
            de.git_repo_manager.ACTION_NAME = 'new_fork'
            spe = _act_spec()
            assert isinstance(spe, dict)
            assert spe
            assert 'project_types' in spe
            assert 'docstring' in spe
        finally:
            de.git_repo_manager.ACTION_NAME = ""

    def test_act_spec_empty_act(self, mocked_app_options):
        mocked_app_options['domain'] = ""
        spe = _act_spec()
        assert isinstance(spe, dict)
        assert spe == dict(local_action=True)

    def test_available_actions(self):
        assert _available_actions()
        assert 'show_status' in _available_actions()
        assert 'new_app' in _available_actions()
        assert 'new_fork' in _available_actions()

    def test_check_arguments(self, mocked_app_options, patched_exit_call_wrapper, reset_patched_globals):
        mocked_app_options['domain'] = ""
        de.git_repo_manager.ACTION_NAME = 'new_app'
        de.git_repo_manager.INI_PDV['project_type'] = PARENT_PRJ
        patched_exit_call_wrapper(_check_arguments)

        de.git_repo_manager.INI_PDV['project_type'] = MODULE_PRJ
        _check_arguments()

        de.git_repo_manager.ACTION_ARGS[:] = ['argument_value']
        patched_exit_call_wrapper(_check_arguments)

        de.git_repo_manager.ACTION_NAME = 'show_repo'
        _check_arguments()

        de.git_repo_manager.ACTION_ARGS[:] = []
        patched_exit_call_wrapper(_check_arguments)

        de.git_repo_manager.INI_PDV['project_type'] = ROOT_PRJ
        de.git_repo_manager.ACTION_NAME = 'install_portions_editable'
        patched_exit_call_wrapper(_check_arguments)

        de.git_repo_manager.ACTION_ARGS[:] = [ARG_ALL]
        _check_arguments()

        de.git_repo_manager.ACTION_ARGS[:] = ['por1', 'por2']
        _check_arguments()

        de.git_repo_manager.INI_PDV['project_type'] = APP_PRJ
        patched_exit_call_wrapper(_check_arguments)

        de.git_repo_manager.ACTION_NAME = 'show_status'
        patched_exit_call_wrapper(_check_arguments)

        de.git_repo_manager.ACTION_NAME = 'invalid_action'
        with pytest.raises(KeyError):
            _check_arguments()

    def test_check_arguments_except_empty_action(self, mocked_app_options):
        mocked_app_options['domain'] = ""
        with pytest.raises(KeyError):
            _check_arguments()

    def test_debug_or_verbose(self, restore_app_env, mocked_app_options):
        assert _debug_or_verbose()
        mocked_app_options['verbose'] = False
        assert _debug_or_verbose()

        old_val = de.git_repo_manager.cae._parsed_arguments
        try:
            de.git_repo_manager.cae._parsed_arguments = True
            assert not _debug_or_verbose()
            mocked_app_options['verbose'] = True
            assert _debug_or_verbose()
        finally:
            de.git_repo_manager.cae._parsed_arguments = old_val

    def test_exit_error(self):
        po, show_help, shutdown = cae.po, cae.show_help, cae.shutdown
        po_args = None

        def _po(*args):
            nonlocal po_args
            po_args = args

        cae.po = _po

        called = 0

        def _sh():
            nonlocal called
            called += 1

        cae.show_help = _sh

        sd_args = None

        def _sd(*args):
            nonlocal sd_args
            sd_args = args

        cae.po = _po
        cae.shutdown = _sd

        _exit_error(3)
        assert po_args is None
        assert called == 1
        assert sd_args == (3, )

        _exit_error(6, 'err')
        assert po_args == ('err', )
        assert called == 2
        assert sd_args == (6, )

        _exit_error(9)
        assert po_args == ('err', )
        assert called == 3
        assert sd_args == (9, )

        # noinspection PyArgumentEqualDefault
        _exit_error(12, "")
        assert po_args == ('err', )
        assert called == 3
        assert sd_args == (12, )

        cae.po, cae.show_help, cae.shutdown = po, show_help, shutdown

    def test_expected_args(self):
        assert _expected_args((('varA_arg1', 'varA_arg2'), ('varB_arg1', 'varB_arg2', 'varB_arg3'))) == \
               "varA_arg1 varA_arg2 or varB_arg1 varB_arg2 varB_arg3"
        assert _expected_args((('a', 'b'), ('c', ), ('d', ))) == "a b or c or d"

    @skip_gitlab_ci
    def test_git_commit_on_changed_repo(self, changed_repo_path, mocked_app_options, patched_exit_call_wrapper):
        files = path_items(os.path.join(changed_repo_path, "*"))
        mocked_app_options['verbose'] = True
        pev = {'project_path': changed_repo_path}

        patched_exit_call_wrapper(_git_commit, pev)
        assert files == path_items(os.path.join(changed_repo_path, "*"))

        write_file(os.path.join(changed_repo_path, 'tst.py'), "# new test file")
        files = path_items(os.path.join(changed_repo_path, "*"))

        patched_exit_call_wrapper(_git_commit, pev)
        assert files == path_items(os.path.join(changed_repo_path, "*"))

        _git_add(pev)
        patched_exit_call_wrapper(_git_commit, pev)
        assert files == path_items(os.path.join(changed_repo_path, "*"))

        write_file(os.path.join(changed_repo_path, COMMIT_MSG_FILE_NAME), "commit title")
        _git_commit(pev)

    @skip_gitlab_ci
    def test_git_commit_on_empty_repo(self, empty_repo_path, mocked_app_options, patched_exit_call_wrapper):
        files = path_items(os.path.join(empty_repo_path, "*"))
        mocked_app_options['verbose'] = True
        pev = {'project_path': empty_repo_path}

        patched_exit_call_wrapper(_git_commit, pev)
        assert files == path_items(os.path.join(empty_repo_path, "*"))

        write_file(os.path.join(empty_repo_path, 'tst.py'), "# new test file")
        patched_exit_call_wrapper(_git_commit, pev)

        write_file(os.path.join(empty_repo_path, COMMIT_MSG_FILE_NAME), "commit message title\n\ncommit message body")
        patched_exit_call_wrapper(_git_commit, pev)

        _git_add(pev)       # adding untracked file tst.py
        _git_commit(pev)

    def test_git_diff_on_changed_repo(self, changed_repo_path, mocked_app_options):
        mocked_app_options['verbose'] = False
        files = path_items(os.path.join(changed_repo_path, "*"))
        pev = {'project_path': changed_repo_path}

        output = _git_diff(pev)
        for file in files:
            if not file.endswith("added.cfg"):
                assert os.path.basename(file) in output
        assert files == path_items(os.path.join(changed_repo_path, "*"))

        mocked_app_options['verbose'] = True

        verbose_output = _git_diff(pev)
        assert len(verbose_output) > len(output)
        for file in files:
            if not file.endswith("added.cfg"):
                assert os.path.basename(file) in verbose_output

    def test_git_diff_on_changed_repo_after_add(self, changed_repo_path, mocked_app_options):
        mocked_app_options['verbose'] = False
        files = path_items(os.path.join(changed_repo_path, "*"))
        pev = {'project_path': changed_repo_path}
        _git_add(pev)
        assert not _git_diff(pev)

    def test_git_diff_on_empty_repo(self, empty_repo_path, mocked_app_options):
        mocked_app_options['verbose'] = False
        files = path_items(os.path.join(empty_repo_path, "*"))
        pev = {'project_path': empty_repo_path}

        output = _git_diff(pev)
        assert files == path_items(os.path.join(empty_repo_path, "*"))

    def test_git_status_on_changed_repo(self, changed_repo_path, mocked_app_options):
        mocked_app_options['verbose'] = False
        files = path_items(os.path.join(changed_repo_path, "*"))
        pev = {'project_path': changed_repo_path}

        output = _git_status(pev)
        for file in files:
            assert os.path.basename(file) in output
        assert files == path_items(os.path.join(changed_repo_path, "*"))

        mocked_app_options['verbose'] = True

        verbose_output = _git_status(pev)
        assert len(verbose_output) > len(output)
        for file in files:
            assert os.path.basename(file) in verbose_output

    def test_git_status_on_empty_repo(self, empty_repo_path, mocked_app_options):
        mocked_app_options['verbose'] = False
        files = path_items(os.path.join(empty_repo_path, "*"))
        pev = {'project_path': empty_repo_path}

        output = _git_status(pev)
        assert files == path_items(os.path.join(empty_repo_path, "*"))

    def test_patch_outsourced_md(self):
        content = "content"
        patcher = "patcher"
        patched_content = _patch_outsourced("any.md", content, patcher)
        assert patched_content.endswith(content)
        assert patched_content.startswith(f"<!-- {OUTSOURCED_MARKER}")
        assert patcher in patched_content

    def test_patch_outsourced_rst(self):
        content = "content"
        patcher = "patcher"
        sep = os.linesep
        patched_content = _patch_outsourced("any.rst", content, patcher)
        assert patched_content.endswith(content)
        assert patched_content.startswith(f"{sep}..{sep}    {OUTSOURCED_MARKER}")
        assert patcher in patched_content

    def test_patch_outsourced_txt(self):
        content = "content"
        patcher = "patcher"
        patched_content = _patch_outsourced("any.txt", content, patcher)
        assert patched_content.endswith(content)
        assert patched_content.startswith(f"# {OUTSOURCED_MARKER}")
        assert patcher in patched_content

    def test_portion_args_package_names_branch_filter(self, mocked_app_options, reset_patched_globals):
        mocked_app_options['branch'] = "filter_branch"
        de.git_repo_manager.ACTION_ARGS = [ARG_ALL]
        de.git_repo_manager.INI_PDV['namespace_name'] = 'n'
        de.git_repo_manager.INI_PDV['portions_project_vars'] = {
            'n_a': {'git_branches': ["any_branch"]}, 'n_b': {'git_branches': ["filter_branch"]}}
        de.git_repo_manager.INI_PDV['portions_packages'] = exp_lst = ['n_a', 'n_b']

        assert _portion_args_package_names() == ['n_b']

    def test_portion_args_package_names_expression(self, mocked_app_options, reset_patched_globals):
        mocked_app_options['branch'] = ""
        de.git_repo_manager.INI_PDV['namespace_name'] = 'n'
        assert not _portion_args_package_names()

        de.git_repo_manager.INI_PDV['portions_presets'] = {}
        de.git_repo_manager.ACTION_ARGS = ["('portion1', ) + ('n_por2', )"]
        assert set(_portion_args_package_names()) == {'n_portion1', 'n_por2'}

        de.git_repo_manager.INI_PDV['portions_presets'] = dict(n_a=['1', 'b'], n_b={'1', 2}, n_c=(1, 2))
        de.git_repo_manager.ACTION_ARGS = ["set(n_a) & (n_b - set(n_c))"]
        assert _portion_args_package_names() == ['n_1']
        de.git_repo_manager.ACTION_ARGS = exp_lst = ["n_a & (n_b - n_c)"]   # fails: list/tuple doesn't support set ops
        assert _portion_args_package_names() == exp_lst

        de.git_repo_manager.ACTION_ARGS = ["n_a ^ (n_b - n_c)"]
        de.git_repo_manager.INI_PDV['portions_presets'] = dict(n_a={'n_1', 'n_b'}, n_b={'n_1', 2}, n_c={1, 2})
        assert _portion_args_package_names() == ['n_b']

        de.git_repo_manager.ACTION_ARGS = ["n_a | (n_b - n_c)"]
        de.git_repo_manager.INI_PDV['portions_presets'] = dict(n_a={'1', 'n_b'}, n_b={'1', 2}, n_c={1, 2})
        assert set(_portion_args_package_names()) == {'n_1', 'n_b'}

        root_path = os.path.join(os.path.dirname(os.getcwd()), 'de')
        if os.path.isdir(root_path):    # test only on local machine with de root project under same parent folder
            de.git_repo_manager.INI_PDV.update(project_dev_vars(project_path=root_path))
            assert de.git_repo_manager.INI_PDV['namespace_name'] == 'de'

            de.git_repo_manager.ACTION_ARGS = ["ALL"]
            all_portions = set(_portion_args_package_names())
            assert all_portions
            de.git_repo_manager.ACTION_ARGS = ["ALL | modified | develop | editable"]
            assert set(_portion_args_package_names()) == all_portions
            de.git_repo_manager.ACTION_ARGS = ["ALL|modified|develop|editable"]
            assert set(_portion_args_package_names()) == all_portions
            de.git_repo_manager.ACTION_ARGS = ["ALL", "|", "modified", "|", "develop", "|", "editable"]
            assert set(_portion_args_package_names()) == all_portions

            de.git_repo_manager.ACTION_ARGS = ["ALL - modified"]
            assert set(_portion_args_package_names()) <= all_portions

            de.git_repo_manager.ACTION_ARGS = ["ALL & modified"]
            assert set(_portion_args_package_names()) <= all_portions

            de.git_repo_manager.ACTION_ARGS = ["ALL ^ modified"]
            assert set(_portion_args_package_names()) <= all_portions

    def test_portion_args_package_names_list(self, mocked_app_options, reset_patched_globals):
        mocked_app_options['branch'] = ""
        de.git_repo_manager.INI_PDV['namespace_name'] = 'n'
        assert not _portion_args_package_names()

        de.git_repo_manager.ACTION_ARGS = [ARG_ALL]
        de.git_repo_manager.INI_PDV['portions_packages'] = exp_lst = ['a1', 'b', 'c3']
        assert set(_portion_args_package_names()) == set(exp_lst)

        de.git_repo_manager.ACTION_ARGS = exp_lst = ['n_a', 'n_b', 'n_c']
        de.git_repo_manager.INI_PDV['portions_presets'] = tst_sets = dict(n_a=[1, 'b', 3], n_b={1, 2, 3}, n_c=('1', 2))
        assert set(_portion_args_package_names()) == set(exp_lst)

        exp_lst = ['n_a', 'n_b', 'n_c']
        de.git_repo_manager.ACTION_ARGS = ['a', 'b', 'c']
        de.git_repo_manager.INI_PDV['portions_presets'] = tst_sets = dict(a=['1', 'b', 3], b={1, 2, 3}, c=('1', 2, 3))
        assert set(_portion_args_package_names()) == set(exp_lst)

    @skip_gitlab_ci
    def test_project_parent_path(self, mocked_app_options, reset_patched_globals):
        assert os.path.basename(_project_parent_path({})) in PARENT_FOLDERS
        assert os.path.basename(_project_parent_path(dict(project_path=os.getcwd()))) in PARENT_FOLDERS

        with in_wd("../../.."):
            assert _project_parent_path({}) == ""

        dn = os.path.dirname
        assert _project_parent_path(dict(project_path=dn(dn(dn(os.getcwd()))))) == ""

        # breaks if gitlab package is pip-installed as -e/editable underneath the 'esc' parent folder
        assert _project_parent_path(dict(project_path=package_project_path('gitlab'))) == ""

    def test_renew_prj_dir(self, mocked_app_options, reset_patched_globals):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        app_name = 'cpl_prj_dir_app'
        project_path = os.path.join(parent_dir, app_name)
        mocked_app_options['group'] = "group_name"
        mocked_app_options['namespace'] = ""
        mocked_app_options['package'] = app_name
        mocked_app_options['path'] = project_path

        try:
            assert not os.path.isdir(project_path)

            _renew_prj_dir("", app_name, project_path, APP_PRJ)

            mocked_app_options['path'] = ""
            _renew_prj_dir("", app_name, project_path, APP_PRJ)

            assert not os.path.isdir(os.path.join(project_path, TEMPLATES_FOLDER))
            _renew_prj_dir(project_path, app_name, project_path, ROOT_PRJ)

            assert os.path.isdir(project_path)
            assert os.path.isdir(os.path.join(project_path, DOCS_FOLDER))
            assert os.path.isdir(os.path.join(project_path, TEMPLATES_FOLDER))
            assert os.path.isdir(os.path.join(project_path, TESTS_FOLDER))
            assert os.path.isfile(os.path.join(project_path, 'main' + PY_EXT))
            assert os.path.isfile(os.path.join(project_path, BUILD_CONFIG_FILE))

            assert not os.path.exists(app_name)
            assert not os.path.exists(BUILD_CONFIG_FILE)

        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_renew_project_ns_portion(self, mocked_app_options, reset_patched_globals):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        ns_name = "nx"
        por_name = "por_name"
        pkg_name = ns_name + '_' + por_name
        project_path = os.path.join(parent_dir, pkg_name)
        root_path = norm_path(os.path.join(parent_dir, ns_name))
        root_req_file = os.path.join(root_path, REQ_DEV_FILE_NAME)
        mocked_app_options['group'] = "group_name"
        mocked_app_options['namespace'] = ns_name
        mocked_app_options['package'] = pkg_name
        mocked_app_options['path'] = project_path
        de.git_repo_manager.REGISTERED_TPL_PROJECTS[ns_name] = dict(path=root_path, version='0.3.6')
        for import_name in TPL_PACKAGES + [ns_name]:    # prevent call of _git_clone() instead of package_project_path()
            mocked_app_options[norm_name(import_name.split('.')[-1]) + TPL_VERSION_OPTION_SUFFIX] = ""

        module_version_file = os.path.join(project_path, ns_name, por_name + PY_EXT)
        sister_prj_path = os.path.join(os.path.dirname(os.getcwd()), pkg_name)
        cur_dir = os.getcwd()
        try:
            os.makedirs(root_path)
            write_file(root_req_file, "")
            os.makedirs(project_path)

            mocked_app_options['package'] = ""
            _renew_project(MODULE_PRJ)
            assert os.path.exists(module_version_file)
            assert read_file(root_req_file) == os.linesep + pkg_name + os.linesep
            assert not os.path.exists(sister_prj_path)

            mocked_app_options['package'] = por_name
            mocked_app_options['path'] = ""
            os.chdir(project_path)
            _renew_project(APP_PRJ)
            os.chdir(cur_dir)
            assert read_file(root_req_file) == os.linesep + pkg_name + os.linesep
            assert not os.path.exists(sister_prj_path)

            assert os.path.isdir(project_path)
            assert not os.path.isdir(os.path.join(project_path, DOCS_FOLDER))
            assert not os.path.isdir(os.path.join(project_path, TEMPLATES_FOLDER))
            assert os.path.isdir(os.path.join(project_path, TESTS_FOLDER))

            assert not os.path.exists(pkg_name)
            assert not os.path.exists(os.path.join(TESTS_FOLDER, pkg_name))

        finally:
            os.chdir(cur_dir)
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_renew_project_change_prj_type(self, mocked_app_options, reset_patched_globals):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        app_name = 'cpl_prj_app_name'
        project_path = os.path.join(parent_dir, app_name)
        mocked_app_options['group'] = "group_name"
        mocked_app_options['namespace'] = ""
        mocked_app_options['package'] = app_name
        mocked_app_options['path'] = project_path
        for import_name in TPL_PACKAGES:    # prevent call of _git_clone() instead of package_project_path()
            mocked_app_options[norm_name(import_name.split('.')[-1]) + TPL_VERSION_OPTION_SUFFIX] = ""

        module_version_file = os.path.join(project_path, app_name + PY_EXT)
        sister_prj_path = os.path.join(os.path.dirname(os.getcwd()), app_name)
        try:
            os.makedirs(project_path)
            write_file(os.path.join(project_path, BUILD_CONFIG_FILE), "")
            _renew_project(MODULE_PRJ)   # project_type change creates module_version_file in project folder
            assert os.path.exists(module_version_file)
            assert not os.path.exists(sister_prj_path)

            os.remove(module_version_file)
            _renew_project(APP_PRJ)
            assert not os.path.exists(module_version_file)
            assert not os.path.exists(sister_prj_path)

            assert os.path.isdir(project_path)
            assert os.path.isdir(os.path.join(project_path, DOCS_FOLDER))
            assert not os.path.isdir(os.path.join(project_path, TEMPLATES_FOLDER))
            assert os.path.isdir(os.path.join(project_path, TESTS_FOLDER))
            assert os.path.isfile(os.path.join(project_path, 'main' + PY_EXT))
            assert os.path.isfile(os.path.join(project_path, BUILD_CONFIG_FILE))

            assert not os.path.exists(app_name)
            assert not os.path.exists(BUILD_CONFIG_FILE)

        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    @skip_gitlab_ci
    def test_renew_project_exits(self, mocked_app_options, patched_exit_call_wrapper, reset_patched_globals):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        app_name = 'tst_app_prj_name'
        project_path = os.path.join(parent_dir, app_name)
        mocked_app_options['branch'] = MAIN_BRANCH
        mocked_app_options['group'] = "group_name"
        mocked_app_options['namespace'] = ""
        mocked_app_options['package'] = app_name
        mocked_app_options['path'] = project_path
        for import_name in TPL_PACKAGES:    # prevent call of _git_clone() instead of package_project_path()
            mocked_app_options[norm_name(import_name.split('.')[-1]) + TPL_VERSION_OPTION_SUFFIX] = ""

        sister_prj_path = os.path.join(os.path.dirname(os.getcwd()), app_name)
        cur_dir = os.getcwd()
        try:
            assert not os.path.isdir(project_path)

            _renew_project(APP_PRJ)
            assert os.path.isdir(project_path)

            assert not os.path.exists(sister_prj_path)
            mocked_app_options['path'] = ""              # NO exit reason, falling back to use package option
            os.chdir(project_path)
            patched_exit_call_wrapper(_renew_project, APP_PRJ)
            os.chdir(cur_dir)
            assert not os.path.exists(sister_prj_path)

            mocked_app_options['package'] = ""           # exit reason
            patched_exit_call_wrapper(_renew_project, APP_PRJ)
            assert not os.path.exists(sister_prj_path)

            mocked_app_options['package'] = app_name
            os.chdir(project_path)
            patched_exit_call_wrapper(_renew_project, APP_PRJ)
            os.chdir(cur_dir)
            assert not os.path.exists(sister_prj_path)

            patched_exit_call_wrapper(_renew_project, ROOT_PRJ)
            assert not os.path.exists(sister_prj_path)

            de.git_repo_manager.INI_PDV['namespace_name'] = ""
            de.git_repo_manager.INI_PDV['project_type'] = APP_PRJ
            de.git_repo_manager.INI_PDV['project_path'] = project_path
            mocked_app_options['group'] = ""  # exit reason
            patched_exit_call_wrapper(_renew_project, APP_PRJ)
            assert not os.path.exists(sister_prj_path)

            # alternative fix to mocked_get_opt_values['repo_group'] = 'group_name'
            de.git_repo_manager.INI_PDV['repo_group'] = "group_name"
            os.chdir(project_path)
            patched_exit_call_wrapper(_renew_project, APP_PRJ)
            os.chdir(cur_dir)
            assert not os.path.exists(sister_prj_path)

            mocked_app_options['path'] = parent_dir  # no parent-dir exit reason
            patched_exit_call_wrapper(_renew_project, APP_PRJ)
            assert not os.path.exists(sister_prj_path)

            assert os.path.isdir(project_path)
            assert os.path.isdir(os.path.join(project_path, DOCS_FOLDER))
            assert not os.path.isdir(os.path.join(project_path, TEMPLATES_FOLDER))
            assert os.path.isdir(os.path.join(project_path, TESTS_FOLDER))
            assert os.path.isfile(os.path.join(project_path, 'main' + PY_EXT))
            assert os.path.isfile(os.path.join(project_path, BUILD_CONFIG_FILE))

        finally:
            os.chdir(cur_dir)
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)


# noinspection PyUnusedLocal
@skip_gitlab_ci  # skip on gitlab because it does not provide git remote user account
class TestHiddenHelpersRemote:
    """ test private helper functions that need authentication against git remote hosts. """
    def test_git_clone_ae_base(self, mocked_app_options, reset_patched_globals):
        import_name = 'ae.base'
        cur_dir = os.getcwd()
        mocked_app_options['verbose'] = False
        # de.git_repo_manager.PRJ_PEV['repo_url'] = repo_root = f"https://gitlab.com/ae-group/{norm_name(import_name)}"

        project_path = _git_clone(f"https://gitlab.com/ae-group", norm_name(import_name))

        tmp_dir = de.git_repo_manager.TEMP_PARENT_FOLDER
        assert project_path.startswith(tmp_dir)
        assert os.path.isdir(os.path.join(tmp_dir, norm_name(import_name)))
        assert os.getcwd() == cur_dir

    # COMMENTED because needs 36 seconds to clone
    # def test_git_clone_gitlab_demo(self, reset_patched_globals):
    #     repo_root = "https://gitlab.com/gitlab-com/demo-systems/sandbox-cloud/apps-tools"
    #     package_name = "hackystack_portal"
    #     cur_dir = os.getcwd()
    #     de.git_repo_manager.PRJ_PEV['repo_url'] = f"{repo_root}/{package_name}"
    #
    #     project_path = _git_clone(repo_root, package_name, "0.3.0")
    #
    #     tmp_dir = de.git_repo_manager.TEMP_PARENT_FOLDER
    #     assert project_path.startswith(tmp_dir)
    #     assert os.path.isdir(os.path.join(tmp_dir, package_name))
    #     assert os.getcwd() == cur_dir

    def test_git_clone_exits(self, mocked_app_options, patched_exit_call_wrapper):
        cur_dir = os.getcwd()
        mocked_app_options['verbose'] = False

        patched_exit_call_wrapper(_git_clone, "_invalid_repo_name", "9.99.999")
        assert os.getcwd() == cur_dir

    def test_prepare_act_exec_info(self, mocked_app_options, reset_patched_globals):
        mocked_app_options['action'] = 'status'
        mocked_app_options['arguments'] = []
        mocked_app_options['domain'] = ''
        mocked_app_options['path'] = ""

        with patch('de.git_repo_manager._register_args_and_local_templates'):
            _prepare_act_exec()

        assert de.git_repo_manager.ACTION_NAME == 'show_status'
        assert not de.git_repo_manager.ACTION_ARGS
        assert not de.git_repo_manager.REMOTE_REPO

    def test_prepare_act_exec_new_app(self, mocked_app_options, reset_patched_globals):
        mocked_app_options['action'] = 'new_app'
        mocked_app_options['arguments'] = []
        mocked_app_options['domain'] = 'github.com'
        mocked_app_options['path'] = ""

        with patch('de.git_repo_manager._register_args_and_local_templates'):
            _prepare_act_exec()

        assert de.git_repo_manager.ACTION_NAME == 'new_app'
        assert not de.git_repo_manager.ACTION_ARGS
        assert not de.git_repo_manager.REMOTE_REPO

    def test_prepare_act_exec_show_repo(self, mocked_app_options, reset_patched_globals):
        mocked_app_options['action'] = 'show_repo'
        mocked_app_options['arguments'] = ['user/repo']
        mocked_app_options['domain'] = 'gitlab.com'
        mocked_app_options['path'] = ""

        with patch('de.git_repo_manager._register_args_and_local_templates'):
            _prepare_act_exec()

        assert de.git_repo_manager.ACTION_NAME == 'show_repo'
        assert de.git_repo_manager.ACTION_ARGS == ['user/repo']
        assert isinstance(de.git_repo_manager.REMOTE_REPO, GitlabCom)

    def test_prepare_act_exec_exits(self, mocked_app_options, patched_exit_call_wrapper, reset_patched_globals):
        mocked_app_options['action'] = 'what_ever_not_existing_action'
        patched_exit_call_wrapper(_prepare_act_exec)

    def test_register_templates(self, mocked_app_options, reset_patched_globals):
        for import_name in TPL_PACKAGES:    # prevent call of _git_clone() instead of package_project_path()
            mocked_app_options[norm_name(import_name.split('.')[-1]) + TPL_VERSION_OPTION_SUFFIX] = ""
        de.git_repo_manager.PRJ_PDV['repo_root'] = repo_root = "https://gitlab.com/ae-group"
        assert not REGISTERED_TPL_PROJECTS

        _register_args_and_local_templates()
        assert REGISTERED_TPL_PROJECTS
